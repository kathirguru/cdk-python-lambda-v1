import json

def handler(event, context):
    print('request: {}'.format(json.dumps(event)))
    return {
        'statusCode': 200,
        'headers': {
            'Content-Type': 'text/plain'
        },
        'body': 'Hello,this is tech exercise URL deployed using CDK Python Lambda to AWS'.format(event['path'])
    }
